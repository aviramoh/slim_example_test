<?php
require "bootstrap.php";
use Exampletestangular2018\Models\User;
use Exampletestangular2018\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all(); //מערך של אוביקטים 
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'name'=>$usr->name,
            'phone_number'=>$usr->phone_number,
        ];        
    }
    return $response->withStatus(200)->withJson($payload);
 });
 
 $app->post('/users', function($request, $response,$args){ //create
    $name = $request->getParsedBodyParam('name', '');
    $phone_number = $request->getParsedBodyParam('phone_number', '');
    $_user = new User();
    $_user->name = $name; 
    $_user->phone_number = $phone_number;
    $_user->save();
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400); 
    } 
 
 });
 
 $app->delete('/users/{user_id}', function($request, $response,$args){ 
   $user = User::find($args['user_id']);
   $user->delete(); 
   if($user->exists){
        return $response->withStatus(400);
    } else {
        return $response->withStatus(200);
    }
 
 });
 
 $app->get('/users/{id}', function($request, $response,$args){
   $_id = $args['id']; 
   $user = User::find($_id); 
   return $response->withStatus(200)->withJson($user);
});

$app->put('/users/{user_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name', '');
	$phone_number = $request->getParsedBodyParam('phone_number', '');
    $_user = User::find($args['user_id']);
    $_user->name = $name;
	$_user->phone_number = $phone_number;
    if($_user->save()){
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    } else{
        return $response->withStatus(400);  
    }
 
 });
 
 $app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', /*'http://mysite'*/'*') // * is all the domains
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();
