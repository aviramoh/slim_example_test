<?php 

namespace Exampletestangular2018\Middleware;

class Logging {
    public function __invoke($request, $response, $next){
        error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.txt");
        $response = $next($request, $response);        
        return $response;
    }
}